package br.com.itau.boleto.services;

import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.*;
import br.com.itau.boleto.repositories.BoletoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDate;

@SpringBootTest
public class TesteBoletoService {

    @MockBean
    private BoletoRepository boletoRepository;

    @MockBean
    private ProdutoService produtoService;

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private CompraService compraService;

    @MockBean
    private ParceiroService parceiroService;

    @Autowired
    private BoletoService boletoService;

    Boleto boleto;
    Parceiro parceiro;
    Cliente cliente;
    Produto produto;
    LocalDate localDate;

    @BeforeEach
    private void setUp(){
        this.localDate = LocalDate.now();

        this.parceiro = new Parceiro();
        parceiro.setCnpj("97.877.256/0001-13");
        parceiro.setEmail("empresa@email.com");
        parceiro.setId(1);
        parceiro.setNome("Empresa 1");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setQuantidadeParcelas(12);

        this.cliente = new Cliente();
        cliente.setCpf("383.755.890-80");
        cliente.setNome("Cliente 1");
        cliente.setEmail("cliente@email.com");
        cliente.setId(1);

        this.boleto = new Boleto();
        boleto.setCodigoDeBarras("0001000100010001502501"
                + String.format("%02d", localDate.getDayOfMonth())
                + String.format("%02d", localDate.getMonthValue())
                + localDate.getYear());
        boleto.setDataDeVencimento(localDate.plusMonths(1));
        boleto.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        boleto.setNumeroParcela(1);
        boleto.setValor(new BigDecimal("150.25"));
        boleto.setParceiro(parceiro);
        boleto.setProduto(produto);
        boleto.setCliente(cliente);
    }

    @Test
    public void testarGerarNovoBoleto(){
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt()))
                .thenReturn(cliente);
        Mockito.when(produtoService.consultarProduto(Mockito.anyInt()))
                .thenReturn(produto);
        Mockito.when(parceiroService.buscarParceiroPorId(Mockito.anyInt()))
                .thenReturn(parceiro);
        Mockito.when(boletoRepository.save(Mockito.any(Boleto.class)))
                .then(object -> object.getArgument(0));

        Assertions.assertEquals(boleto.getCodigoDeBarras(), boletoService.gerarNovoBoleto(1, 1, 1, 150.25, 1).getCodigoDeBarras().substring(4));
    }

    @Test
    public void testarPagarBoleto(){
        Mockito.when(boletoRepository.findByCodigoDeBarras(Mockito.anyString())).thenReturn(boleto);
        Mockito.when(boletoRepository.save(Mockito.any(Boleto.class))).then(object -> object.getArgument(0));

        Assertions.assertNotEquals(boleto.getSituacao(), boletoService.pagarBoleto(Mockito.anyString()).getSituacao());
    }

    @Test
    public void testarPagarBoletoJaPago(){
        boleto.setSituacao(Situacao.PAGO);
        Mockito.when(boletoRepository.findByCodigoDeBarras(Mockito.anyString())).thenReturn(boleto);

        Assertions.assertThrows(RuntimeException.class, () -> {boletoService.pagarBoleto(Mockito.anyString());});
    }
}
