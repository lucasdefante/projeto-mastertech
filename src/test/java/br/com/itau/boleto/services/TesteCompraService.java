package br.com.itau.boleto.services;

import br.com.itau.boleto.DTOs.CompraDTO;
import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.*;
import br.com.itau.boleto.repositories.CompraRepository;
import com.sun.org.apache.xpath.internal.objects.XObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class TesteCompraService {

    @MockBean
    private BoletoService boletoService;

    @MockBean
    private CompraRepository compraRepository;

    @Autowired
    private CompraService compraService;

    Boleto boleto;
    List<Boleto> boletoList;
    Parceiro parceiro;
    Cliente cliente;
    Produto produto;
    LocalDate localDate;
    Compra compra;
    CompraDTO compraDTO;

    @BeforeEach
    private void setUp(){
        this.localDate = LocalDate.now();

        this.parceiro = new Parceiro();
        parceiro.setCnpj("97.877.256/0001-13");
        parceiro.setEmail("empresa@email.com");
        parceiro.setId(1);
        parceiro.setNome("Empresa 1");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setQuantidadeParcelas(12);

        this.cliente = new Cliente();
        cliente.setCpf("383.755.890-80");
        cliente.setNome("Cliente 1");
        cliente.setEmail("cliente@email.com");
        cliente.setId(1);

        this.boleto = new Boleto();
        boleto.setCodigoDeBarras("0001000100010001502501"
                + String.format("%02d", localDate.getDayOfMonth())
                + String.format("%02d", localDate.getMonthValue())
                + localDate.getYear());
        boleto.setDataDeVencimento(localDate.plusMonths(1));
        boleto.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        boleto.setNumeroParcela(1);
        boleto.setValor(new BigDecimal("150.25"));
        boleto.setParceiro(parceiro);
        boleto.setProduto(produto);
        boleto.setCliente(cliente);
        boleto.setId(1);

        this.boletoList = new ArrayList<>();
        boletoList.add(boleto);

        this.compra = new Compra();
        compra.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        compra.setBoletos(boletoList);
        compra.setQuantidadeParcelas(1);
        compra.setValorDaCompra(new BigDecimal("150.25"));

        this.compraDTO = new CompraDTO();
        compraDTO.setQuantidadeParcelas(1);
        compraDTO.setValorDaCompra(new BigDecimal("150.25"));
    }

    @Test
    public void testarNovaCompra(){
        Mockito.when(boletoService.gerarNovoBoleto(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyDouble(), Mockito.anyInt()))
                .thenReturn(boleto);

        Mockito.when(compraRepository.save(Mockito.any(Compra.class))).then(object -> object.getArgument(0));

        Assertions.assertEquals(compra, compraService.novaCompra(compraDTO, 1, 1, 1));

    }

    @Test
    public void testarAtualizarCompraPago(){
        compra.getBoletos().get(0).setSituacao(Situacao.PAGO);
        Mockito.when(compraRepository.findByBoletosId(Mockito.anyInt())).thenReturn(Optional.of(compra));
        Mockito.when(compraRepository.save(Mockito.any(Compra.class))).then(object -> object.getArgument(0));

        Assertions.assertEquals(Situacao.PAGO, compraService.atualizarCompra(Mockito.anyInt()).getSituacao());
    }

    @Test
    public void testarAtualizarCompraEmAtraso(){
        compra.getBoletos().get(0).setSituacao(Situacao.EM_ATRASO);
        Mockito.when(compraRepository.findByBoletosId(Mockito.anyInt())).thenReturn(Optional.of(compra));
        Mockito.when(compraRepository.save(Mockito.any(Compra.class))).then(object -> object.getArgument(0));

        Assertions.assertEquals(Situacao.EM_ATRASO, compraService.atualizarCompra(Mockito.anyInt()).getSituacao());
    }

    @Test
    public void testarAtualizarCompraPendenteDePagamento(){
        compra.getBoletos().get(0).setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        Mockito.when(compraRepository.findByBoletosId(Mockito.anyInt())).thenReturn(Optional.of(compra));
        Mockito.when(compraRepository.save(Mockito.any(Compra.class))).then(object -> object.getArgument(0));

        Assertions.assertEquals(Situacao.PENDENTE_DE_PAGAMENTO, compraService.atualizarCompra(Mockito.anyInt()).getSituacao());
    }
}
