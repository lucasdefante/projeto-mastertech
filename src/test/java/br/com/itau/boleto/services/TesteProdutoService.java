package br.com.itau.boleto.services;

import br.com.itau.boleto.models.Produto;
import br.com.itau.boleto.repositories.ProdutoRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TesteProdutoService {

    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private ProdutoService produtoService;

    Produto produto;

    @Test
    public void testarSalvarParceiro() {
        Mockito.when(produtoService.salvarProduto(produto)).then(objeto -> objeto.getArgument(0));
    }
}
