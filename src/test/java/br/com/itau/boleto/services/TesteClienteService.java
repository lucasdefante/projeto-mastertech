package br.com.itau.boleto.services;

import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class TesteClienteService {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;
    List<Cliente> clientes;

    @BeforeEach
    private void setUp() {
        this.cliente = new Cliente();
        cliente.setCpf("58920742006");
        cliente.setEmail("joaquim@gmail.com");
        cliente.setNome("Manoel Joaquim");
        cliente.setId(1);

        this.clientes = new ArrayList<>();
    }

    @Test
    public void testarBuscaDeClientePeloID() {
        Optional<Cliente> clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        Cliente clienteOBjeto = clienteService.buscarClientePorID(1);
        Assertions.assertEquals(cliente, clienteOBjeto);
        Assertions.assertEquals(cliente, clienteService.buscarClientePorID(1));
    }

    @Test
    public void testarBuscaDeClientePeloIDQueNaoExiste() {
        Optional<Cliente> clienteOptional = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            clienteService.buscarClientePorID(312);
        });
    }

    @Test
    public void testarSalvarLead() {

        Mockito.when(clienteRepository.findAll()).thenReturn(this.clientes);


        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).then(objeto -> objeto.getArgument(0));

        Cliente testeDeCliente = clienteService.cadastrarCliente(cliente);

        Assertions.assertEquals("Manoel Joaquim", testeDeCliente.getNome());

    }
}
