package br.com.itau.boleto.services;

import br.com.itau.boleto.models.Parceiro;
import br.com.itau.boleto.repositories.ParceiroRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class TesteParceiroService {

    @MockBean
    private ParceiroRepository parceiroRepository;

    @MockBean
    private ParceiroService parceiroService;

    Parceiro parceiro;

    @BeforeEach
    public void setUp(){
        this.parceiro = new Parceiro();
        parceiro.setNome("Teste da silva");
        parceiro.setCnpj("07.599.795/0001-45");
        parceiro.setEmail("empresa@empresa.com.br");
    }

    @Test
    public void testarSalvarParceiro(){
        Mockito.when(parceiroService.salvarParceiro(parceiro)).then(objeto -> objeto.getArgument(0));
    }

//    @Test
//    public void testarbuscarParceiroPorId(){
//        Optional<Parceiro> parceiroOptional = Optional.of(parceiro);
//        parceiro.setId(1);
//        Mockito.when(parceiroRepository.findById(Mockito.anyInt())).thenReturn(parceiroOptional);
//        Parceiro parceiroObjeto = parceiroService.buscarParceiroPorId(1);
//        Assertions.assertEquals(parceiro, parceiroObjeto);
//        Assertions.assertEquals(parceiro, parceiroService.buscarParceiroPorId(1));
//    }

}
