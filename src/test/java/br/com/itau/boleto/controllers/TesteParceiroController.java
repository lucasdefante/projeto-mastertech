package br.com.itau.boleto.controllers;

import br.com.itau.boleto.auth.JWTUtil;
import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.models.Parceiro;
import br.com.itau.boleto.services.ParceiroService;
import br.com.itau.boleto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ParceiroController.class)
@Import(JWTUtil.class)
public class TesteParceiroController {

    @MockBean
    private ParceiroService parceiroService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Parceiro parceiro;

    @BeforeEach
    public void setUp(){
        parceiro = new Parceiro();
        parceiro.setCnpj("86.061.414/0001-46");
        parceiro.setNome("Empresa Parceira");
        parceiro.setEmail("empresa@email.com");
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarCadastrarCliente() throws Exception {
        Mockito.when(parceiroService.salvarParceiro(Mockito.any(Parceiro.class))).thenReturn(parceiro);
        ObjectMapper objectMapper = new ObjectMapper();

        String parceiroJson = objectMapper.writeValueAsString(parceiro);

        mockMvc.perform(MockMvcRequestBuilders.post("/parceiros")
                .contentType(MediaType.APPLICATION_JSON).content(parceiroJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Empresa Parceira")));
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarBuscarPorID() throws Exception {
        parceiro.setId(1);
        Mockito.when(parceiroService.buscarParceiroPorId(Mockito.anyInt())).thenReturn(parceiro);

        mockMvc.perform(MockMvcRequestBuilders.get("/parceiros/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

}
