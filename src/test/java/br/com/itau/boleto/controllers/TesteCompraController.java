package br.com.itau.boleto.controllers;

import br.com.itau.boleto.DTOs.CompraDTO;
import br.com.itau.boleto.auth.JWTUtil;
import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.*;
import br.com.itau.boleto.repositories.CompraRepository;
import br.com.itau.boleto.services.CompraService;
import br.com.itau.boleto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(CompraController.class)
@Import(JWTUtil.class)
public class TesteCompraController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompraRepository compraRepository;

    @MockBean
    private CompraService compraService;

    @MockBean
    private UsuarioService usuarioService;

    Boleto boleto;
    List<Boleto> boletoList;
    Parceiro parceiro;
    Cliente cliente;
    Produto produto;
    LocalDate localDate;
    Compra compra;
    CompraDTO compraDTO;

    @BeforeEach
    private void setUp(){
        this.localDate = LocalDate.now();

        this.parceiro = new Parceiro();
        parceiro.setCnpj("97.877.256/0001-13");
        parceiro.setEmail("empresa@email.com");
        parceiro.setId(1);
        parceiro.setNome("Empresa 1");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setQuantidadeParcelas(12);

        this.cliente = new Cliente();
        cliente.setCpf("383.755.890-80");
        cliente.setNome("Cliente 1");
        cliente.setEmail("cliente@email.com");
        cliente.setId(1);

        this.boleto = new Boleto();
        boleto.setCodigoDeBarras("0001000100010001502501"
                + String.format("%02d", localDate.getDayOfMonth())
                + String.format("%02d", localDate.getMonthValue())
                + localDate.getYear());
        boleto.setDataDeVencimento(localDate.plusMonths(1));
        boleto.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        boleto.setNumeroParcela(1);
        boleto.setValor(new BigDecimal("150.25"));
        boleto.setParceiro(parceiro);
        boleto.setProduto(produto);
        boleto.setCliente(cliente);
        boleto.setId(1);

        this.boletoList = new ArrayList<>();
        boletoList.add(boleto);

        this.compra = new Compra();
        compra.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        compra.setBoletos(boletoList);
        compra.setQuantidadeParcelas(1);
        compra.setValorDaCompra(new BigDecimal("150.25"));

        this.compraDTO = new CompraDTO();
        compraDTO.setQuantidadeParcelas(1);
        compraDTO.setValorDaCompra(new BigDecimal("150.25"));
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarCadastrarNovaCompra() throws Exception{
        Mockito.when(compraService.novaCompra(Mockito.any(CompraDTO.class), Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt()))
        .thenReturn(compra);

        ObjectMapper objectMapper = new ObjectMapper();
        String compraJson = objectMapper.writeValueAsString(compraDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/compras/1/1/1")
                .contentType(MediaType.APPLICATION_JSON).content(compraJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.boletos[0].cliente.nome", CoreMatchers.equalTo("Cliente 1")));
    }

}
