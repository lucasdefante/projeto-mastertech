package br.com.itau.boleto.controllers;

import br.com.itau.boleto.auth.JWTUtil;
import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.services.ClienteService;
import br.com.itau.boleto.services.UsuarioService;
import ch.qos.logback.core.net.server.Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ClienteController.class)
@Import(JWTUtil.class)
public class TesteClienteController {

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Cliente cliente;
    List<Cliente> clientes;

    @BeforeEach
    private void setUp() {
        this.cliente = new Cliente();
        cliente.setCpf("58920742006");
        cliente.setEmail("joaquim@gmail.com");
        cliente.setNome("Manoel Joaquim");
        cliente.setId(1);

        this.clientes = new ArrayList<>();
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarBuscarPorID() throws Exception {

        Mockito.when(clienteService.buscarClientePorID(1))
                .thenReturn(cliente);

        mockMvc.perform(MockMvcRequestBuilders.get("/clientes/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/clientes/" + cliente.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .isBadRequest());
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarLerTodosOsClientes() throws Exception {

        Mockito.when(clienteService.buscarTodosClientes()).thenReturn(clientes);

        mockMvc.perform(MockMvcRequestBuilders.get("/clientes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarCadastrarCliente() throws Exception {
        Mockito.when(clienteService.cadastrarCliente(Mockito.any(Cliente.class)))
                .thenReturn(cliente);
        ObjectMapper objectMapper = new ObjectMapper();

        String clienteJson = objectMapper.writeValueAsString(cliente);

        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .contentType(MediaType.APPLICATION_JSON).content(clienteJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Manoel Joaquim")));
    }
}
