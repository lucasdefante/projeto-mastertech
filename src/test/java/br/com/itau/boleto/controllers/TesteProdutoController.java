package br.com.itau.boleto.controllers;

import br.com.itau.boleto.auth.JWTUtil;
import br.com.itau.boleto.models.Produto;
import br.com.itau.boleto.services.ProdutoService;
import br.com.itau.boleto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(ProdutoController.class)
@Import(JWTUtil.class)
public class TesteProdutoController {

    @MockBean
    private ProdutoService produtoService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Produto produto;

    @BeforeEach
    public void setUp() {
        this.produto = new Produto();
        produto.setId(99);
        produto.setNome("nome do produto 99");
        produto.setDescricao("descricao produto 99");
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarCadastrarProduto() throws Exception {

        Mockito.when(produtoService.salvarProduto(produto)).thenReturn(produto);

        ObjectMapper objectMapper = new ObjectMapper();

        String parceiroJson = objectMapper.writeValueAsString(produto);
        ResultActions $ = mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON).content(parceiroJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome", CoreMatchers.containsString("nome do produto 99")));
    }
}
