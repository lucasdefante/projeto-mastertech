package br.com.itau.boleto.controllers;

import br.com.itau.boleto.auth.JWTUtil;
import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.Boleto;
import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.models.Parceiro;
import br.com.itau.boleto.models.Produto;
import br.com.itau.boleto.services.BoletoService;
import br.com.itau.boleto.services.UsuarioService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDate;

@WebMvcTest(BoletoController.class)
@Import(JWTUtil.class)
public class TesteBoletoController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BoletoService boletoService;

    @MockBean
    private UsuarioService usuarioService;

    Boleto boleto;
    Parceiro parceiro;
    Cliente cliente;
    Produto produto;
    LocalDate localDate;

    @BeforeEach
    public void setUp() {
        this.localDate = LocalDate.now();

        this.parceiro = new Parceiro();
        parceiro.setCnpj("97.877.256/0001-13");
        parceiro.setEmail("empresa@email.com");
        parceiro.setId(1);
        parceiro.setNome("Empresa 1");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setQuantidadeParcelas(12);

        this.cliente = new Cliente();
        cliente.setCpf("383.755.890-80");
        cliente.setNome("Cliente 1");
        cliente.setEmail("cliente@email.com");
        cliente.setId(1);

        this.boleto = new Boleto();
        boleto.setCodigoDeBarras("0001000100010001502501"
                + String.format("%02d", localDate.getDayOfMonth())
                + String.format("%02d", localDate.getMonthValue())
                + localDate.getYear());
        boleto.setDataDeVencimento(localDate.plusMonths(1));
        boleto.setSituacao(Situacao.PAGO);
        boleto.setNumeroParcela(1);
        boleto.setValor(new BigDecimal("150.25"));
        boleto.setParceiro(parceiro);
        boleto.setProduto(produto);
        boleto.setCliente(cliente);
        boleto.setId(1);
    }

    @Test
    @WithMockUser(username = "mauricio@gmail.com", password = "mauricio")
    public void testarPagarBoleto() throws Exception{
        Mockito.when(boletoService.pagarBoleto(Mockito.anyString())).thenReturn(boleto);

        mockMvc.perform(MockMvcRequestBuilders.post("/boletos/11122223333444455332"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.cliente.nome", CoreMatchers.equalTo("Cliente 1")));
    }
}
