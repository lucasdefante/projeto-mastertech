package br.com.itau.boleto.services;

import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.Boleto;
import br.com.itau.boleto.repositories.BoletoRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Date;
import java.util.Random;

@Service
public class BoletoService {

    @Autowired
    private BoletoRepository boletoRepository;

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private CompraService compraService;

    @Autowired
    private ParceiroService parceiroService;

    @Autowired
    private ClienteService clienteService;

    public Boleto gerarNovoBoleto(int id_parceiro, int id_produto, int id_cliente, double valorBoleto, int numeroParcela) {
        LocalDate localDate = LocalDate.now();
        Boleto boleto = new Boleto();
        boleto.setCliente(clienteService.buscarClientePorID(id_cliente));
        boleto.setProduto(produtoService.consultarProduto(id_produto));
        boleto.setParceiro(parceiroService.buscarParceiroPorId(id_parceiro));
        boleto.setValor(new BigDecimal(valorBoleto).setScale(2, RoundingMode.HALF_EVEN));
        boleto.setNumeroParcela(numeroParcela);
        boleto.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);
        boleto.setDataDeVencimento(localDate.plusMonths((long) numeroParcela));
        boleto.setCodigoDeBarras(gerarCodigoDeBarras(id_parceiro, id_produto, id_cliente, valorBoleto, numeroParcela, localDate));

        return boletoRepository.save(boleto);
    }

    public String gerarCodigoDeBarras(int id_parceiro, int id_produto, int id_cliente, double valorBoleto, int numeroParcela, LocalDate localDate) {
        valorBoleto *= 100;
        Random rnd = new Random();
        String codigoDeBarras =
                  String.format("%04d", rnd.nextInt(1000))
                + String.format("%04d", id_parceiro)
                + String.format("%04d", id_produto)
                + String.format("%04d", id_cliente)
                + String.format("%08d", (int) valorBoleto)
                + String.format("%02d", numeroParcela)
                + String.format("%02d", localDate.getDayOfMonth())
                + String.format("%02d", localDate.getMonthValue())
                + String.valueOf(localDate.getYear());

        return codigoDeBarras;
    }

    public Boleto pagarBoleto(String codigoDeBarras) {
        Boleto boleto = boletoRepository.findByCodigoDeBarras(codigoDeBarras);
        LocalDate date = LocalDate.now();

        if (boleto.getSituacao() == Situacao.PAGO) {
            throw new RuntimeException("Boleto não está pendente de pagamento. Solicitação não atendida.");
        }
        
        boleto.setSituacao(Situacao.PAGO);
        boleto.setDataDePagamento(date);
        Boleto boletoDB = boletoRepository.save(boleto);

        compraService.atualizarCompra(boletoDB.getId());

        return boletoDB;
    }

    public Iterable<Boleto> retornarTodosBoletosPendentesDePagamento(){
        return boletoRepository.findBySituacao(Situacao.PENDENTE_DE_PAGAMENTO);
    }

    public Boleto atualizarBoleto(Boleto boleto){
        return boletoRepository.save(boleto);
    }
}
