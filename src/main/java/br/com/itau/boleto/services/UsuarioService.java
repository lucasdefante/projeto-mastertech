package br.com.itau.boleto.services;

import br.com.itau.boleto.auth.AuthUsuario;
import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.models.Usuario;
import br.com.itau.boleto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Usuario salvarUsuario (Usuario usuario) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String senha = usuario.getSenha();
        usuario.setSenha(encoder.encode(senha));

        return usuarioRepository.save(usuario);
    }

    public Iterable<Usuario> lerTodosOsUsuarios() {
        return usuarioRepository.findAll();
    }

    public Usuario buscarUsuarioPorID (int id) {

//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        String senha = usuario.getSenha();
//        usuario.setSenha(encoder.encode(senha));

        Optional<Usuario> usuarioOptional = usuarioRepository.findById((long) id);
        if (usuarioOptional.isPresent()) {
            Usuario usuario = usuarioOptional.get();
            return usuario;
        } else {
            throw new RuntimeException("O usuário não foi encontrado");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null) {
            throw new UsernameNotFoundException("O email: " + email + " não foi encontrado");
        }
        AuthUsuario authUsuario = new AuthUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return authUsuario;
    }
}
