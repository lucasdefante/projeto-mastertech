package br.com.itau.boleto.services;

import br.com.itau.boleto.models.Produto;
import br.com.itau.boleto.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Produto consultarProduto(int id) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);

        if (produtoOptional.isPresent()) {
            Produto produto = produtoOptional.get();
            return produto;
        } else {
            throw new RuntimeException("Produto não existe");
        }
    }

    public Produto atualizarProduto(int id, Produto produto) {

        Produto produtoDB = consultarProduto(id);
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Produto não existe");
        }
    }
}
