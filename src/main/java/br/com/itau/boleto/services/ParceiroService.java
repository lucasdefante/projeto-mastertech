package br.com.itau.boleto.services;

import br.com.itau.boleto.models.Parceiro;
import br.com.itau.boleto.repositories.ParceiroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ParceiroService {

    @Autowired
    private ParceiroRepository parceiroRepository;


    public Parceiro salvarParceiro(Parceiro parceiro){
        Parceiro objetoParceiro = parceiroRepository.save(parceiro);
        return objetoParceiro;
    }


    public Parceiro buscarParceiroPorId(int id){
        Optional<Parceiro> parceiroOptional = parceiroRepository.findById(id);

        if(parceiroOptional.isPresent()){
            Parceiro parceiro = parceiroOptional.get();
            return parceiro;
        }else {
            throw new RuntimeException("O parceiro não foi encontrado ");
        }
    }


    // Atualizar
    public Parceiro atualizarParceiro(int id, Parceiro parceiro){
        Parceiro parceiroDB = buscarParceiroPorId(id);
        parceiro.setId(parceiroDB.getId());
        return parceiroRepository.save(parceiro);
    }

    // Deletar
    public void deletarParceiro(int id){
        if(parceiroRepository.existsById(id)) {
            parceiroRepository.deleteById(id);
        }else {
            throw new RuntimeException("Não foi localizado o registro do parceiro na base de dados");
        }
    }

}
