package br.com.itau.boleto.DTOs;

import br.com.itau.boleto.models.Compra;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CompraDTO {

    @NotNull(message = "Valor da compra não pode ser nulo")
    @Digits(integer = 500000, fraction = 2, message = "Valor da compra deve seguir o formato 'X.XX' com 2 casas decimais e valor máximo de 500 mil")
    private BigDecimal valorDaCompra;

    @NotNull(message = "Quantidade de parcelas não pode ser nulo")
    @Min(value = 1, message = "Quantidade de parcelas deve ser maior que zero")
    private int quantidadeParcelas;

    public CompraDTO() {
    }

    public BigDecimal getValorDaCompra() {
        return valorDaCompra;
    }

    public void setValorDaCompra(BigDecimal valorDaCompra) {
        this.valorDaCompra = valorDaCompra;
    }

    public int getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(int quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public Compra converterParaCompra(){
        Compra compra = new Compra();
        compra.setValorDaCompra(this.valorDaCompra);
        compra.setQuantidadeParcelas(this.quantidadeParcelas);

        return compra;
    }
}
