package br.com.itau.boleto.schedulers;

import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.Boleto;
import br.com.itau.boleto.services.BoletoService;
import br.com.itau.boleto.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@EnableScheduling
public class VerificadorDeBoletos {

    @Autowired
    private BoletoService boletoService;

    @Autowired
    private CompraService compraService;

    private static final String TIME_ZONE = "America/Sao_Paulo";

    @Scheduled(cron = "0 0 0 * * *", zone = TIME_ZONE)
    public void verificaPorDia(){
        LocalDate hoje = LocalDate.now();
        System.out.println("Scheduled: "+hoje);
        List<Boleto> boletos = (List) boletoService.retornarTodosBoletosPendentesDePagamento();
        for (Boleto boleto:
             boletos) {
            if(hoje.isAfter(boleto.getDataDeVencimento())){
                boleto.setSituacao(Situacao.EM_ATRASO);
                boletoService.atualizarBoleto(boleto);
                compraService.atualizarCompra(boleto.getId());
            }
        }
    }
}
