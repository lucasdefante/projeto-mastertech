package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Compra;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CompraRepository extends CrudRepository<Compra, Integer> {
    Optional<Compra> findByBoletosId(int idBoleto);
}
