package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
//    Object findAll(Iterable<Object> anyIterable);
}
