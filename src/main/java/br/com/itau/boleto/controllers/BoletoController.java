package br.com.itau.boleto.controllers;

import br.com.itau.boleto.models.Boleto;
import br.com.itau.boleto.services.BoletoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/boletos")
public class BoletoController {

    @Autowired
    private BoletoService boletoService;

    @PostMapping("/{codigoDeBarras}")
    @ResponseStatus(HttpStatus.OK)
    public Boleto pagarBoleto(@PathVariable(name = "codigoDeBarras") String codigoDeBarras){
        try {
            return boletoService.pagarBoleto(codigoDeBarras);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
