package br.com.itau.boleto.controllers;

import br.com.itau.boleto.models.Produto;
import br.com.itau.boleto.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto registrarProduto(@RequestBody @Valid Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Produto pesquisarProdutoPorId(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.consultarProduto(id);
            return produto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id") int id) {
        try {
            Produto produtoAtualizado = produtoService.atualizarProduto(id, produto);
            return produtoAtualizado;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id) {
        try {
            produtoService.deletarProduto(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
