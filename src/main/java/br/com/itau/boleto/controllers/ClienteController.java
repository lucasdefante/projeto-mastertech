package br.com.itau.boleto.controllers;

import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente) {
        return clienteService.cadastrarCliente(cliente);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Cliente> buscarTodosClientes() {
        Iterable<Cliente> cliente = clienteService.buscarTodosClientes();
        return cliente;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente buscarClientePorID(@PathVariable(name = "id") int id) {
        try {
            Cliente cliente = clienteService.buscarClientePorID(id);
            return cliente;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente atualizarCliente(@RequestBody @Valid Cliente cliente, @PathVariable(name = "id") int id) {
        try {
            Cliente clienteAtualizado = clienteService.atualizarCliente(id, cliente);
            return clienteAtualizado;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarCliente(@PathVariable(name = "id") int id) {
        try {
            clienteService.deletarCliente(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
