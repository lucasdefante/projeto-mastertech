package br.com.itau.boleto.controllers;

import br.com.itau.boleto.models.Cliente;
import br.com.itau.boleto.models.Usuario;
import br.com.itau.boleto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody @Valid Usuario usuario) {
        return usuarioService.salvarUsuario(usuario);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Usuario> listarUsuarios() {
        return usuarioService.lerTodosOsUsuarios();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Usuario buscarUsuarioPorID(int id) {

        try {
            Usuario usuario = usuarioService.buscarUsuarioPorID(id);
            return usuario;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

