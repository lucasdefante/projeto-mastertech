package br.com.itau.boleto.models;

import br.com.itau.boleto.enums.Situacao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Entity
public class Compra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany
    private List<Boleto> boletos;

    private BigDecimal valorDaCompra;

    private int quantidadeParcelas;

    private Situacao situacao;

    public Compra() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Boleto> getBoletos() {
        return boletos;
    }

    public void setBoletos(List<Boleto> boletos) {
        this.boletos = boletos;
    }

    public BigDecimal getValorDaCompra() {
        return valorDaCompra;
    }

    public void setValorDaCompra(BigDecimal valorDaCompra) {
        this.valorDaCompra = valorDaCompra;
    }

    public int getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(int quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compra compra = (Compra) o;
        return id == compra.id &&
                quantidadeParcelas == compra.quantidadeParcelas &&
                Objects.equals(boletos, compra.boletos) &&
                Objects.equals(valorDaCompra, compra.valorDaCompra) &&
                situacao == compra.situacao;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, boletos, valorDaCompra, quantidadeParcelas, situacao);
    }

    @Override
    public String toString() {
        return "Compra{" +
                "id=" + id +
                ", boletos=" + boletos +
                ", valorDaCompra=" + valorDaCompra +
                ", quantidadeParcelas=" + quantidadeParcelas +
                ", situacao=" + situacao +
                '}';
    }
}
