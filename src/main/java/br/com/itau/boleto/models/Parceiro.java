package br.com.itau.boleto.models;

import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
public class Parceiro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser em branco")
    @Size(min = 3, message = "Nome precisa ter mais de 3 caracteres")
    private String nome;

    @CNPJ(message = "CNPJ inválido")
    @NotNull(message = "CNPJ não pode ser nulo")
    private String cnpj;

    @Email(message = "E-mail inválido")
    @NotNull(message = "E-mail não pode ser nulo")
    private String email;

    public Parceiro() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parceiro parceiro = (Parceiro) o;
        return id == parceiro.id &&
                Objects.equals(nome, parceiro.nome) &&
                Objects.equals(cnpj, parceiro.cnpj) &&
                Objects.equals(email, parceiro.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cnpj, email);
    }

    @Override
    public String toString() {
        return "Parceiro{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cnpj='" + cnpj + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
