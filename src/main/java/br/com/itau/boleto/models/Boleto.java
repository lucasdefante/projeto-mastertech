package br.com.itau.boleto.models;

import br.com.itau.boleto.enums.Situacao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Boleto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private Cliente cliente;

    @OneToOne
    private Produto produto;

    @OneToOne
    private Parceiro parceiro;

    private String codigoDeBarras;

    private LocalDate dataDeVencimento;

    private BigDecimal valor;

    private int numeroParcela;

    private Situacao situacao;

    private LocalDate dataDePagamento;

    public Boleto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Parceiro getParceiro() {
        return parceiro;
    }

    public void setParceiro(Parceiro parceiro) {
        this.parceiro = parceiro;
    }

    public String getCodigoDeBarras() {
        return codigoDeBarras;
    }

    public void setCodigoDeBarras(String codigoDeBarras) {
        this.codigoDeBarras = codigoDeBarras;
    }

    public LocalDate getDataDeVencimento() {
        return dataDeVencimento;
    }

    public void setDataDeVencimento(LocalDate dataDeVencimento) {
        this.dataDeVencimento = dataDeVencimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public int getNumeroParcela() {
        return numeroParcela;
    }

    public void setNumeroParcela(int numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public LocalDate getDataDePagamento() {
        return dataDePagamento;
    }

    public void setDataDePagamento(LocalDate dataDePagamento) {
        this.dataDePagamento = dataDePagamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Boleto boleto = (Boleto) o;
        return id == boleto.id &&
                numeroParcela == boleto.numeroParcela &&
                Objects.equals(cliente, boleto.cliente) &&
                Objects.equals(produto, boleto.produto) &&
                Objects.equals(parceiro, boleto.parceiro) &&
                Objects.equals(codigoDeBarras, boleto.codigoDeBarras) &&
                Objects.equals(dataDeVencimento, boleto.dataDeVencimento) &&
                Objects.equals(valor, boleto.valor) &&
                situacao == boleto.situacao &&
                Objects.equals(dataDePagamento, boleto.dataDePagamento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cliente, produto, parceiro, codigoDeBarras, dataDeVencimento, valor, numeroParcela, situacao, dataDePagamento);
    }

    @Override
    public String toString() {
        return "Boleto{" +
                "id=" + id +
                ", cliente=" + cliente +
                ", produto=" + produto +
                ", parceiro=" + parceiro +
                ", codigoDeBarras='" + codigoDeBarras + '\'' +
                ", dataDeVencimento=" + dataDeVencimento +
                ", valor=" + valor +
                ", numeroParcela=" + numeroParcela +
                ", situacao=" + situacao +
                ", dataDePagamento=" + dataDePagamento +
                '}';
    }
}
