package br.com.itau.boleto.configurations;

import br.com.itau.boleto.auth.FiltroDeAutenticacao;
import br.com.itau.boleto.auth.FiltroDeAutorizacao;
import br.com.itau.boleto.auth.JWTUtil;

import br.com.itau.boleto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UsuarioService usuarioService;

    private static final String[] ENDERECOS_PUBLICOS_METODO_POST = {
            "/login",
            "/usuarios"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.cors();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests().antMatchers(HttpMethod.POST, ENDERECOS_PUBLICOS_METODO_POST).permitAll()
                .anyRequest().authenticated();

        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));

        http.addFilter(new FiltroDeAutorizacao(authenticationManager(), jwtUtil, usuarioService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioService).passwordEncoder(construtorDeEncoder());
    }

    @Bean
    CorsConfigurationSource construtorDeCors() {
        UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();

        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return cors;
    }
    @Bean
    BCryptPasswordEncoder construtorDeEncoder() {
        return new BCryptPasswordEncoder();
    }
}
